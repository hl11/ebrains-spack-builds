## Experimental release: Process to create new Spack installation root in case we want to update the Spack release

**the process contains many manual steps**

1. In the Gitlab repository go to CI/CD > Variables > Expand and set the appropriate {site}_INSTALLATION_ROOT_{env} variable to the **new name**
2. Start a new OKD job with [$OP == "manualFixing"] from CLI with `oc create -f simplejob.yml`
3. Connect to the running pod to get access to the NFS partition
4. Execute `mkdir -p {site}_INSTALLATION_ROOT_{env}`
5. Execute `cd {site}_INSTALLATION_ROOT_{env}`
6. Execute all commands of [$OP == "create"] from "ebrains-spack-build-env>bin/deploy-build-env.sh". 
- Right after the git clone command, Set the git commit of the Spack instance to the desired one.
- Right after the previous action go to "spack/etc/spack" and create "packages.yaml" with content:
```
packages:
  all:
    target: [x86_64]
```
- Right after the `spack compiler find` command install also the appropriate python with `spack install python@3.8.11 %gcc@10.3.0`
7. Create the appropriate LAB_KERNEL_PATH if it does not exist
