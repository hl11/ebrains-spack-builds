#!/bin/bash

# ===========================================================================================================
# title         : create_job.sh
# usage         : ./create_job.sh $OC_JOB_ID $BUILD_ENV_DOCKER_IMAGE $INSTALLATION_ROOT $SPACK_VERSION
#                 $SPACK_ENV $BRANCH $RELEASE_NAME $LAB_KERNEL_ROOT
# description   : creates OKD job yaml file that builds/updates spack environment and creates Lab kernel
# ===========================================================================================================

OC_JOB_ID=$1
BUILD_ENV_DOCKER_IMAGE=$2
INSTALLATION_ROOT=$3
SPACK_VERSION=$4
EBRAINS_SPACK_ENV=$5
BRANCH=$6
RELEASE_NAME=$7
LAB_KERNEL_ROOT=$8

cat <<EOT >> simplejob.yml
apiVersion: batch/v1
kind: Job
metadata:
  name: simplejob${OC_JOB_ID}
spec:
  parallelism: 1
  completions: 1
  backoffLimit: 0
  template:
    metadata:
      name: testjob
    spec:
      containers:
      - name: simplejob
        image: ${BUILD_ENV_DOCKER_IMAGE}
        imagePullPolicy: Always
        resources:
          limits:
            cpu: '6'
            memory: '18Gi'
          requests:
            cpu: '4'
            memory: '12Gi'
        volumeMounts:
          - name: sharedbin
            mountPath: /srv
          - name: tmp
            mountPath: /tmp
        command:
        - /bin/bash
        - -c
        - |
          # create root dir if it doesn't exist
          mkdir -p \$INSTALLATION_ROOT
          
          # reset build error log dir (delete previous logs to save space)
          rm -rf \$BUILD_LOGS_DIR
          mkdir -p \$BUILD_LOGS_DIR
          
          # reset spack repository dir by cloning the selected version
          rm -rf \$EBRAINS_REPO_PATH
          git clone https://gitlab.ebrains.eu/technical-coordination/project-internal/devops/platform/ebrains-spack-builds.git --branch \$BRANCH \$EBRAINS_REPO_PATH
          
          # run installation script
          . \$EBRAINS_REPO_PATH/install_spack_env.sh \$SPACK_JOBS \$INSTALLATION_ROOT \$SPACK_VERSION \$EBRAINS_REPO_PATH \$EBRAINS_SPACK_ENV
          
          if [ \$? -eq 0 ]
          then
            # build process succeeded - create or update kernel on the NFS based on the current spack environment
            chmod +x \$EBRAINS_REPO_PATH/create_JupyterLab_kernel.sh
            \$EBRAINS_REPO_PATH/create_JupyterLab_kernel.sh \$INSTALLATION_ROOT \$EBRAINS_SPACK_ENV \$RELEASE_NAME \$LAB_KERNEL_ROOT
            exit 0
          else
            # build process failed - keep spack build logs and fail the pipeline
            cp -r /tmp/spack/spack-stage/* \$SPACK_BUILD_LOGS
            exit
          fi
        env:
          - name: SYSTEMNAME
            value: ebrainslab
          - name: INSTALLATION_ROOT
            value: $INSTALLATION_ROOT
          - name: SPACK_VERSION
            value: $SPACK_VERSION
          - name: EBRAINS_SPACK_ENV
            value: $EBRAINS_SPACK_ENV
          - name: BRANCH
            value: $BRANCH
          - name: RELEASE_NAME
            value: $RELEASE_NAME
          - name: LAB_KERNEL_ROOT
            value: $LAB_KERNEL_ROOT
          - name: BUILD_LOGS_DIR
            value: /srv/build_logs/$EBRAINS_SPACK_ENV
          - name: EBRAINS_REPO_PATH
            value: $INSTALLATION_ROOT/ebrains-spack-builds
          - name: SPACK_JOBS
            value: '4'
      volumes:
        - name: sharedbin
          persistentVolumeClaim:
            claimName: shared-binaries
        - name: tmp
          emptyDir: {}
      restartPolicy: Never
EOT
