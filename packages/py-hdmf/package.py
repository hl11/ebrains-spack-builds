# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class PyHdmf(PythonPackage):
    """The Hierarchical Data Modeling Framework."""

    homepage = "https://github.com/hdmf-dev/hdmf"
    pypi = "hdmf/hdmf-3.4.6.tar.gz"

    maintainers = ["rly"]

    version("3.4.6", sha256="e2fe8a1139bd06787df61b2fac830e1a4359eef60a70d82567d3509609103ca3")

    depends_on("py-setuptools", type="build")

    depends_on("py-h5py@2.10:", type=("build", "run"))
    depends_on("py-jsonschema@2.6.0:", type=("build", "run"))
    depends_on("py-numpy@1.16:", type=("build", "run"))
    depends_on("py-pandas@1.0.5:", type=("build", "run"))
    depends_on("py-ruamel-yaml@0.16:", type=("build", "run"))
    depends_on("py-scipy@1.1:", type=("build", "run"))
