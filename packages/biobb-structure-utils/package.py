# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *

class BiobbStructureUtils(PythonPackage):
    """Biobb_structure_utils is the Biobb module collection to modify 
    or extract information from a PDB structure file."""

    # Homepage and download url
    homepage = "https://github.com/bioexcel/biobb_structure_utils"
    git = 'https://github.com/bioexcel/biobb_structure_utils.git'
    url = 'https://github.com/bioexcel/biobb_structure_utils/archive/refs/tags/v4.0.0.tar.gz'

    # Set the gitlab accounts of this package maintainers
    maintainers = ['dbeltran']

    # Versions
    version('master', branch='master')
    version('4.0.0', sha256='f328eee2166631e1cc514118eb41187620c358c8024431ce2a0b0e4547692c47')

    # Dependencies
    depends_on('python@3.8:', type=('build', 'run'))
    depends_on('biobb-common')
    depends_on('biobb-structure-checking')

    # Patching to enable python 3.10 (not official, might not be stable)
    def patch(self):
        filter_file("    python_requires='>=3.7,<=3.10',", "    python_requires='>=3.7,<3.11',", "setup.py")
        filter_file(
            "'Programming Language :: Python :: 3.9'",
            "'Programming Language :: Python :: 3.9',\r\n        "
            "'Programming Language :: Python :: 3.10'",
            "setup.py",
        )

    # Test
    @run_after('install')
    @on_package_attributes(run_tests=True)
    def check_install (self):
        python("-c", 'import biobb_structure_utils')
