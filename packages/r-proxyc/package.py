# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *


class RProxyc(RPackage):
    """Computes proximity between rows or columns of large matrices efficiently in C++. Functions are optimised for large sparse matrices using the Armadillo and Intel TBB libraries. Among several built-in similarity/distance measures, computation of correlation, cosine similarity and Euclidean distance is particularly fast."""

    homepage = "https://cran.r-project.org/package=proxyC"
    cran = "proxyC"

    version("0.3.3", sha256="3e25da94633da0b5c6cdac03f5b6d8205d078f60c5a09073e90a1b6f0930e302")

    depends_on("r-rcpp")
    depends_on("r-rcppparallel")
    depends_on("r-rcpparmadillo")
    depends_on("r-matrix")
