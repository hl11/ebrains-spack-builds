# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class PyTvbData(PythonPackage):
    """
    Various demonstration datasets for use with The Virtual Brain are provided here.
    """

    homepage = "https://zenodo.org/record/8331301"
    url = 'https://zenodo.org/record/8331301/files/tvb_data.zip'

    maintainers = ['paulapopa', "ldomide"]

    version('2.8', 'd2f9b5933327c1d106838d301a4bf7b5')
    version('2.7', 'f74ec53edadb4540da3de7268257dd20')
    version('2.0.3', '1e02cdc21147f46644c57b14429f564f')

    # python_requires
    depends_on('python@3.8:3.10', type=('build', 'run'))

    # setup_requires
    depends_on('py-pip', type='build')
    depends_on('py-setuptools', type=('build'))

    # this is only a data holder package, thus no real python code exists inside.
    @run_after('install')
    @on_package_attributes(run_tests=True)
    def install_test(self):

        with working_dir('spack-test', create=True):
            python('-c',
                   'import tvb_data; '
                   'import os; '
                   'RR = tvb_data.__path__[0]; '
                   'assert os.path.exists(os.path.join(RR, "Default_Project.zip")); '
                   'assert os.path.exists(os.path.join(RR, "connectivity"))'
                   '')
